package main

import (
	"github.com/gin-gonic/gin"
	"jatis-evoucher/common"
	rm "jatis-evoucher/merchant/routers"
	rp "jatis-evoucher/program/routers"
	rv "jatis-evoucher/voucher/routers"
)

func main() {
	common.InitDB()

	r := gin.Default()
	v1 := r.Group("/api")
	rm.MerchantRoute(v1.Group("/merchant"))
	rm.DepositRoute(v1.Group("/deposit"))
	rp.ProgramRoutes(v1.Group("/program"))
	rv.ProgramRoutes(v1.Group("/voucher"))



	r.Run(":5555")
}

