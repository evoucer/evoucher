package controller

import (
	"github.com/zpnk/go-bitly"
)

func GenerateBitly(voucherID string) (string,error) {
	b := bitly.New("b299f50d5be0a6b4adb02efc533673dfe900f1ac")
	shortUrl, err := b.Links.Shorten("http://127.0.0.1:5555/api/voucher/" + voucherID)

	if err != nil {
		return "",err
	}
	return shortUrl.URL,nil
}