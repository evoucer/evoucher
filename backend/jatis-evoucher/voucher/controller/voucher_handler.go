package controller

import (
	"github.com/satori/go.uuid"
	"github.com/gin-gonic/gin"

	"jatis-evoucher/voucher/entity"
	"jatis-evoucher/voucher/models"

	modelsProgram "jatis-evoucher/program/models"

	"encoding/csv"
	"os"
	"bufio"
	"io"
	"fmt"
	"net/http"
	"time"
)

func Generate(c *gin.Context) {
	var voucher entity.Voucher

	//mengambil data program untuk menentukan CSVpath dan ter n condition
	programID := c.Param("programID")
	program,errP := modelsProgram.GetProgram(programID)
	if errP != nil {
		fmt.Println(errP)
		return
	}

	csvFile,err := os.Open(program.Csv_path)
	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusBadRequest,gin.H{"status":http.StatusBadRequest,"data":"no such file or directory"})
		return
	}
	reader := csv.NewReader(bufio.NewReader(csvFile))

	for{
		data, err := reader.Read()
		if err == io.EOF {
			break
		} else if err != nil{
			fmt.Println(err)
			c.JSON(http.StatusBadRequest,gin.H{"status":http.StatusBadRequest,"data":nil})
			return
		}
		//menentukan id voucher
		u1,_ := uuid.NewV1()
		voucher.Voucher_id = u1.String()
		voucher.Program_id = programID
		//menentukan cust_no dan message dari csv
		voucher.Cust_no = data[0]
		//menentukan bitly url
		voucher.Url,_ = GenerateBitly(voucher.Voucher_id)
		//menentukan message
		voucher.Message = string(program.Tnc + " " + voucher.Url)
		errModel := models.CreateVoucher(voucher)
		if errModel != nil {
			fmt.Println(errModel)
			c.JSON(http.StatusBadRequest,gin.H{"status":http.StatusBadRequest,"data":nil})
			return
		}
	}
	c.JSON(http.StatusOK,gin.H{"status":http.StatusOK,"data":"create voucher successful"})
}

func GetVoucher(c *gin.Context)  {
	voucherID := c.Param("voucherID")

	result,err := models.GetVoucher(voucherID)
	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusBadRequest,gin.H{"status":http.StatusBadRequest,"data":nil})
		return
	}

	if !result.Redeem {
		c.JSON(http.StatusBadRequest,gin.H{"status":http.StatusBadRequest,"data":"voucher has been redeemed"})
		return
	}

	checkDate,_ := modelsProgram.GetProgram(result.Program_id)
	period_end, err := time.Parse("2006-01-02 15:04:05", checkDate.Periode_end)
	if err != nil {
		fmt.Println(err)
		return
	}
	if time.Now().After(period_end) {
		c.JSON(http.StatusBadRequest,gin.H{"status":http.StatusBadRequest,"data":"voucher has been expired"})
		return
	}

	c.JSON(http.StatusOK,gin.H{"status":http.StatusOK,"data":result})
}

func RedeemVoucher(c *gin.Context) {
	var voucher entity.Voucher
	voucherID := c.Param("voucherID")
	c.BindJSON(&voucher)
	//flag
	voucher.Redeem = false
	err := models.UpdateVoucher(voucher,voucherID)
	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusBadRequest,gin.H{"status":http.StatusBadRequest,"data":nil})
		return
	}
	c.JSON(http.StatusOK,gin.H{"status":http.StatusOK,"data":"voucher redeem successful"})
}

func GetFileCsv(c *gin.Context) {
	programID := c.Param("programID")
	dataCsv := [][]string{}
	dataVoucher := []string{}
	result,_ := models.GetDataCsv(programID)
	for index,_ := range result{
		dataVoucher = []string{result[index].Cust_no,result[index].Message}
		dataCsv = append(dataCsv,dataVoucher)
	}

	file, _:= os.Create("result/result.csv") //output path file
	w := csv.NewWriter(file)
	w.WriteAll(dataCsv) // calls Flush internally
	if err := w.Error(); err != nil {
		fmt.Println("error writing csv:", err)
		c.JSON(http.StatusBadRequest,gin.H{"status":http.StatusBadRequest,"data":"error writing csv"})
		return
	}
	c.JSON(http.StatusOK,gin.H{"status":http.StatusOK,"data":"success"})
}