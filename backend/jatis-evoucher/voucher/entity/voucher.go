package entity

import "time"

type Voucher struct {
	Voucher_id string `json:"voucher_id"`
	Program_id string `json:"program_id"`
	Redeem bool `json:"redeem"`
	Merchant_branch_code string `json:"merchant_branch_code"`
	Cust_name string `json:"cust_name"`
	Cust_id string `json:"cust_id"`
	Cust_no string `json:"cust_no"`
	Message string `json:"message"`
	Url string `json:"url"`
	Created time.Time `json:"created"`
	Updated time.Time `json:"updated"`
}
