package routers

import (
	"github.com/gin-gonic/gin"
	"jatis-evoucher/voucher/controller"
)

func ProgramRoutes(router *gin.RouterGroup)  {
	router.GET("/:voucherID",controller.GetVoucher)
	router.POST("/generate/:programID",controller.Generate)
	router.PUT("/redeem/:voucherID",controller.RedeemVoucher)
	router.POST("/csv/:programID",controller.GetFileCsv)
}