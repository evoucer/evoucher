package models

import (
	"jatis-evoucher/voucher/entity"
	"jatis-evoucher/common/global"
	"errors"
	"time"
)

func GetVoucher(id interface{}) (entity.Voucher,error) {
	var voucher entity.Voucher
	_, err := global.DBconn.Query(&voucher, `SELECT * FROM voucher WHERE voucher_id=?`,id)
	if err != nil {
		return voucher,err
	}
	if voucher.Voucher_id == "" {
		return voucher, errors.New("data not found")
	}
	return voucher, nil
}

func UpdateVoucher(voucher entity.Voucher, id interface{}) error {
	voucher.Updated = time.Now()
	_, err := global.DBconn.QueryOne(voucher,`UPDATE voucher SET redeem=?,
	merchant_branch_code=?,
	cust_name=?,
	cust_id=?,
	updated=? WHERE voucher_id=?`,
		voucher.Redeem,
		voucher.Merchant_branch_code,
		voucher.Cust_name,
		voucher.Cust_id,
		voucher.Updated,
		id)
	if err != nil {
		return err
	}
	return nil
}

func CreateVoucher(voucher entity.Voucher) error {
	voucher.Created = time.Now()
	voucher.Updated = time.Now()
	voucher.Redeem = true

	_,err := global.DBconn.QueryOne(voucher,`INSERT INTO voucher VALUES(?,?,?,?,?,?,?,?,?,?,?)`,
		voucher.Voucher_id,
		voucher.Program_id,
		voucher.Redeem,
		voucher.Merchant_branch_code,
		voucher.Cust_name,
		voucher.Cust_id,
		voucher.Cust_no,
		voucher.Message,
		voucher.Url,
		voucher.Created,
		voucher.Updated)
	if err != nil {
		return err
	}
	return nil
}

func GetDataCsv(programId interface{}) ([]entity.Voucher,error) {
	var voucher []entity.Voucher
	_, err := global.DBconn.Query(&voucher, `SELECT * FROM voucher WHERE program_id=?`,programId)
	if err != nil {
		return voucher,err
	}
	if voucher[0].Voucher_id == "" {
		return voucher, errors.New("data not found")
	}
	return voucher, nil
}