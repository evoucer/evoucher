package routers_voucher

import (
	"github.com/gin-gonic/gin"
	"jatis-evoucher/voucher/controller"
)

func ProgramRoutes(router *gin.RouterGroup)  {
	router.GET("/:voucherID",controller.GetVoucher)
	router.POST("/generate/:programID",controller.Generate)
	router.POST("/redeem/:voucherID",controller.RedeemVoucher)
}