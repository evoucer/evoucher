package entity

import (
	"time"
)

type Program struct {
	Program_id string `json:"program_id"`
	Program_name string `json:"program_name"`
	Client_id string `json:"client_id"`
	Merchant_id string `json:"merchant_id"`
	Periode_start string `json:"periode_start"`
	Periode_end string `json:"periode_end"`
	Url string `json:"url"`
	Logo_path string `json:"logo_path"`
	Tnc string `json:"tnc"`
	Csv_path string `json:"csv_path"`
	Voucher_value int `json:"voucher_value"`
	Created time.Time `json:"created"`
	Updated time.Time `json:"updated"`
}