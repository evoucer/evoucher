package controller

import (
	"github.com/gin-gonic/gin"
	"jatis-evoucher/program/models"
	"jatis-evoucher/program/entity"
	"net/http"
	"fmt"
)

func GetProgram(c *gin.Context)  {
	id := c.Param("programID")

	result,err := models.GetProgram(id)
	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusBadRequest,gin.H{"status":http.StatusBadRequest,"data":nil})
		return
	}
	c.JSON(http.StatusOK,gin.H{"status":http.StatusOK,"data":result})

}

func UpdateProgram(c *gin.Context) {
	var program entity.Program
	id := c.Param("programID")
	c.BindJSON(&program)

	err := models.UpdateProgram(program,id)
	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusBadRequest,gin.H{"status":http.StatusBadRequest,"data":nil})
		return
	}
	c.JSON(http.StatusOK,gin.H{"status":http.StatusOK,"data":"update data successful"})

}

func CreateProgram(c *gin.Context)  {
	var program entity.Program
	c.BindJSON(&program)

	err := models.CreateProgram(program)
	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusBadRequest,gin.H{"status":http.StatusBadRequest,"data":nil})
		return
	}
	c.JSON(http.StatusOK,gin.H{"status":http.StatusOK,"data":"create data successful"})
}

func DeleteProgram(c *gin.Context)  {
	id := c.Param("programID")
	err := models.DeleteProgram(id)
	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusBadRequest,gin.H{"status":http.StatusBadRequest,"data":nil})
		return
	}
	c.JSON(http.StatusOK,gin.H{"status":http.StatusOK,"data":"delete data successful"})
}