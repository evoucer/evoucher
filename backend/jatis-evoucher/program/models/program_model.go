package models

import (
	"jatis-evoucher/program/entity"
	"jatis-evoucher/common/global"
	"time"
	"errors"
)

func GetProgram(programId interface{}) (entity.Program,error) {
	var program entity.Program
	_, err := global.DBconn.Query(&program, `SELECT * FROM program where program_id=?`, programId)
	if err != nil {
		return program, err
	}
	if program.Program_id == "" {
		return program,errors.New("data not found")
	}
	return program, nil
}

func CreateProgram(data entity.Program) error {
	data.Created = time.Now()
	data.Updated = time.Now()
	//parsing time peridde start. format : yyyy-mm-dd hh:mm:ss
	periode_start, err := time.Parse("2006-01-02 15:04:05", data.Periode_start)
	if err != nil {
		return err
	}
	//parsing time perido end. format : yyyy-mm-dd hh:mm:ss
	period_end, err := time.Parse("2006-01-02 15:04:05", data.Periode_end)
	if err != nil {
		return err
	}

	_, err = global.DBconn.QueryOne(data, `INSERT INTO program VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)`,
		data.Program_id,
		data.Program_name,
		data.Client_id,
		data.Merchant_id,
		periode_start,
		period_end,
		//data.Periode_start,
		//data.Periode_end,
		data.Url,
		data.Logo_path,
		data.Tnc,
		data.Csv_path,
		data.Voucher_value,
		data.Created,
		data.Updated)
	if err != nil {
		return err
	}
	return nil
}

func UpdateProgram(data entity.Program, programId interface{}) error {
	data.Updated = time.Now()
	_, err := global.DBconn.QueryOne(&data, `UPDATE program SET periode_end=?,
	url=?,
	logo_path=?,
	tnc=?,
	csv_path=?,
	voucher_value=?,
	updated=? WHERE program_id=?`,
		data.Periode_end,
		data.Url,
		data.Logo_path,
		data.Tnc,
		data.Csv_path,
		data.Voucher_value,
		data.Updated,
		programId)
	if err != nil {
		return err
	}
	return nil
}

func DeleteProgram(programId interface{}) error {
	var program entity.Program
	_,errGet := GetProgram(programId)
	if errGet != nil {
		return errGet
	}
	_, err := global.DBconn.Query(program, `DELETE FROM program WHERE program_id=?`,programId)
	if err != nil {
		return err
	}
	return nil
}
