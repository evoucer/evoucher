package routers

import (
	"github.com/gin-gonic/gin"
	"jatis-evoucher/program/controller"
)

func ProgramRoutes(router *gin.RouterGroup)  {
	router.GET("/:programID", controller.GetProgram)
	router.POST("/create", controller.CreateProgram)
	router.PUT("/update/:programID", controller.UpdateProgram)
	router.DELETE("/delete/:programID", controller.DeleteProgram)
}
