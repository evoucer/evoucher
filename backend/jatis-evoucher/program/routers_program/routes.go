package routers_program

import (
	"github.com/gin-gonic/gin"
	"jatis-evoucher/program/controller"
)

func ProgramRoutes(router *gin.RouterGroup)  {
	router.GET("/:id", controller.GetProgram)
	router.POST("/create", controller.CreateProgram)
	router.POST("/update/:id", controller.UpdateProgram)
	router.DELETE("/delete/:id", controller.DeleteProgram)
}
