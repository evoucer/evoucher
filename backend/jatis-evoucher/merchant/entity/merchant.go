package entity

import (
	"time"
)

type Merchant struct {
	Merchant_id string `json:"merchant_id"`
	Merchant_name string `json:"merchant_name"`
	Contact_person string `json:"contact_person"`
	Address string `json:"address"`
	Created time.Time `json:"created"`
	Updated time.Time `json:"updated"`
}
