package entity

import (
	"time"
)

type Deposit struct {
	Deposit_id string `json:"deposit_id"`
	Merchant_id string `json:"merchant_id"`
	Topup int `json:"topup"`
	Topup_date time.Time `json:"topup_date"`
	Created time.Time `json:"created"`
	Updated time.Time `json:"updated"`
}
