package models

import (
	"jatis-evoucher/merchant/entity"
	"jatis-evoucher/common/global"
	"errors"
	"time"
)

func GetMerchants() ([]entity.Merchant, error) {
	var merchants []entity.Merchant
	_, err := global.DBconn.Query(&merchants, `SELECT * FROM merchant`)
	//err := global.DBconn.Model(&merchants).Select()
	if err != nil {
		return merchants, err
	}
	return merchants, nil
}

func GetMerchant(id interface{}) (entity.Merchant, error) {
	var merchant entity.Merchant
	_, err := global.DBconn.Query(&merchant,`SELECT * FROM merchant WHERE merchant_id=?`,id)
	if err != nil {
		return merchant, err
	}
	if merchant.Merchant_id == "" {
		return merchant, errors.New("data not found")
	}
	return merchant, nil
}

func CreateMerchant(data entity.Merchant) error {
	data.Created = time.Now()
	data.Updated = time.Now()

	_, err := global.DBconn.QueryOne(data, `INSERT INTO merchant VALUES(?merchant_id,?merchant_name,?contact_person,?address,?created,?updated)`,
		data)

	if err != nil {
		return err
	}
	return nil
}

func UpdateMerchant(data entity.Merchant, id interface{}) error {
	data.Updated = time.Now()
	_, err := global.DBconn.QueryOne(data,
		`UPDATE merchant SET contact_person=?,address=?,updated=? WHERE merchant_id=?`,
		data.Contact_person,data.Address,data.Updated,id)

	if err != nil {
		return err
	}
	return nil
}

func DeleteMerchant(data entity.Merchant,id interface{}) error {
	_,err := global.DBconn.Query(data, `DELETE FROM merchant WHERE merchant_id=?`,id)

	if err != nil {
		return err
	}
	return nil
}
