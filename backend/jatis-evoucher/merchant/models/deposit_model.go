package models

import (
	"jatis-evoucher/merchant/entity"
	"jatis-evoucher/common/global"
	"github.com/satori/go.uuid"
	"time"
	"fmt"
)

func GetDeposit(id interface{}) (entity.Deposit, error) {
	var deposit entity.Deposit
	_, err := global.DBconn.Query(&deposit, `SELECT * FROM deposit WHERE merchant_id=?`,id)
	if err != nil {
		return deposit, err
	}
	return deposit, nil
}

func CreateDeposit(data entity.Deposit) error {
	u1,errUUID := uuid.NewV1()
	if errUUID != nil {
		fmt.Println(errUUID)
	}
	data.Deposit_id = u1.String()
	data.Created = time.Now()
	data.Updated = time.Now()

	_,err := global.DBconn.QueryOne(data,
		`INSERT INTO deposit VALUES(?deposit_id,?merchant_id,?topup,?topup_date,?created,?updated)`,
		data)

	if err != nil {
		return err
	}
	return nil
}

func UpdateDeposit(data entity.Deposit, id interface{}) error {
	data.Topup_date = time.Now()
	data.Updated = time.Now()

	_,err := global.DBconn.QueryOne(data,
		`UPDATE deposit SET topup=?,topup_date=?,updated=? WHERE merchant_id=?`,
		data.Topup, data.Topup_date, data.Updated, id)

	if err != nil {
		return err
	}
	return nil
}

func DeleteDeposit(data entity.Deposit, id interface{}) error {
	_,err := global.DBconn.Query(data, `DELETE FROM deposit WHERE merchant_id=?`,id)

	if err != nil {
		return err
	}
	return nil
}

