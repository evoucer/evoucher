package controller

import (
	"github.com/gin-gonic/gin"
	_"jatis-evoucher/merchant/entity"
	"jatis-evoucher/merchant/models"
	"fmt"
	"net/http"
	"jatis-evoucher/merchant/entity"
)

func GetDeposit(c *gin.Context)  {
	id := c.Param("merchantID")

	result, err := models.GetDeposit(id)
	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusBadRequest,gin.H{"status":http.StatusBadRequest,"data":nil})
		return
	}
	c.JSON(http.StatusOK,gin.H{"status":http.StatusOK,"data":result})
}

func CreateDeposit(c *gin.Context)  {
	var data entity.Deposit
	c.BindJSON(&data)

	err := models.CreateDeposit(data)
	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusBadRequest,gin.H{"status":http.StatusBadRequest,"data":nil})
		return
	}
	c.JSON(http.StatusOK,gin.H{"status":http.StatusOK,"data":"create data successful"})
}

func TopupDeposit(c *gin.Context)  {
	var data entity.Deposit
	id := c.Param("merchantID")
	c.BindJSON(&data)

	err := models.UpdateDeposit(data, id)
	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusBadRequest,gin.H{"status":http.StatusBadRequest,"data":nil})
		return
	}
	c.JSON(http.StatusOK,gin.H{"status":http.StatusOK,"data":"topup successful"})
}

func DeleteDeposit(c *gin.Context)  {
	var data entity.Deposit
	id := c.Param("merchantID")

	err := models.DeleteDeposit(data, id)
	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusBadRequest,gin.H{"status":http.StatusBadRequest,"data":nil})
		return
	}
	c.JSON(http.StatusOK,gin.H{"status":http.StatusOK,"data":"delete data successful"})
}
