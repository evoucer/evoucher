package controller

import (
	"jatis-evoucher/merchant/models"
	"github.com/gin-gonic/gin"
	"fmt"
	"net/http"
	"jatis-evoucher/merchant/entity"
)

func GetAllMerchant(c *gin.Context)  {
	data,err := models.GetMerchants()
	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusBadRequest,gin.H{"status":http.StatusBadRequest,"data":err.Error()})
		return
	}
	c.JSON(http.StatusOK,gin.H{"status":http.StatusOK,"data":data})

}

func GetMerchant(c *gin.Context) {
	id := c.Param("merchantID")
	data, err := models.GetMerchant(id)
	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusBadRequest,gin.H{"status":http.StatusBadRequest,"data":err.Error()})
		return
	}
	c.JSON(http.StatusOK,gin.H{"status":http.StatusOK,"data":data})
}

func CreateMerchant(c *gin.Context) {
	var data entity.Merchant
	c.BindJSON(&data)

	err := models.CreateMerchant(data)
	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusBadRequest,gin.H{"status":http.StatusBadRequest,"data":err.Error()})
		return
	}
	c.JSON(http.StatusOK,gin.H{"status":http.StatusOK,"data":"create data successful"})
}

func UpdateMerchant(c *gin.Context) {
	var data entity.Merchant
	id := c.Param("merchantID")
	c.BindJSON(&data)

	err := models.UpdateMerchant(data, id)
	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusBadRequest,gin.H{"status":http.StatusBadRequest,"data":err.Error()})
		return
	}
	c.JSON(http.StatusOK,gin.H{"status":http.StatusOK,"data":"update data successful"})
}

func DeleteMerchant(c *gin.Context) {
	var data entity.Merchant
	id := c.Param("merchantID")

	err := models.DeleteMerchant(data, id)
	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusBadRequest,gin.H{"status":http.StatusBadRequest,"data":err.Error()})
		return
	}
	c.JSON(http.StatusOK,gin.H{"status":http.StatusOK,"data":"delete data successful"})
}