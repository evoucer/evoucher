package routers_merchant

import (
	"github.com/gin-gonic/gin"
	"jatis-evoucher/merchant/controller"
)

func MerchantRoute(router *gin.RouterGroup)  {
	router.GET("/", controller.GetAllMerchant)
	router.GET("/:id", controller.GetMerchant)
	router.POST("/create", controller.CreateMerchant)
	router.POST("/update/:id", controller.UpdateMerchant)
	router.DELETE("/delete/:id", controller.DeleteMerchant)
}

func DepositRoute(router *gin.RouterGroup)  {
	router.GET("/:id",controller.GetDeposit)
	router.POST("/create",controller.CreateDeposit)
	router.POST("/topup/:id", controller.TopupDeposit)
	router.DELETE("/delete/:id", controller.DeleteDeposit)
}
