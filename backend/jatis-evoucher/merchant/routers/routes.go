package routers

import (
	"github.com/gin-gonic/gin"
	"jatis-evoucher/merchant/controller"
)

func MerchantRoute(router *gin.RouterGroup)  {
	router.GET("/", controller.GetAllMerchant)
	router.GET("/:merchantID", controller.GetMerchant)
	router.POST("/create", controller.CreateMerchant)
	router.PUT("/update/:merchantID", controller.UpdateMerchant)
	router.DELETE("/delete/:merchantID", controller.DeleteMerchant)
}

func DepositRoute(router *gin.RouterGroup)  {
	router.GET("/:merchantID",controller.GetDeposit)
	router.POST("/create",controller.CreateDeposit)
	router.PUT("/topup/:merchantID", controller.TopupDeposit)
	router.DELETE("/delete/:merchantID", controller.DeleteDeposit)
}
