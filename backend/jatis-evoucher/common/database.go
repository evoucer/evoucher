package common

import (
	"jatis-evoucher/common/global"
	"github.com/go-pg/pg"
	"fmt"
)

func InitDB() {
	 global.DBconn = pg.Connect(&pg.Options{
		User: "postgres",
		Password: "postgres",
		Database: "evoucherdb",
	})

	var n int
	_, err := global.DBconn.QueryOne(pg.Scan(&n), "SELECT 1")
	if err != nil {
		panic(err)
	}

	fmt.Println(n)
}